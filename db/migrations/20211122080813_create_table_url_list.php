<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTableUrlList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('url_list', ['id' => true, 'primary_key' => 'id']);

        $table->changeColumn('id', 'integer', ['identity' => true])
            ->addColumn('title', 'string', ['null' => false, 'limit' => 255])
            ->addColumn('url', 'string', ['null' => false, 'limit' => 255])
            ->addColumn('is_active', 'integer', ['default' => '1'])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['title'], ['unique' => true, 'name' => 'idx_title'])
            ->addIndex(['url'], ['unique' => true, 'name' => 'idx_url'])
            ->create();
    }
}
