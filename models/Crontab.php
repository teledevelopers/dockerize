<?php

namespace app\models;

use yii\db\ActiveRecord;

class Crontab extends ActiveRecord
{
    public static function getAll()
    {
        $tasksArray = self::find()
            ->asArray()
            ->all();
        $tasks = '';

        foreach ($tasksArray as $task) {
            $taskFormat = "# %s\n" . ($task['active'] === '0' ? '# ' : '') . "%s %s";
            $tasks .= "\n\n";
            $tasks .= sprintf(
                $taskFormat,
                $task['comment'],
                $task['time'],
                $task['task']
            );
        }

        return $tasks;
    }
}