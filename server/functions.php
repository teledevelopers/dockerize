<?php

/**
 * Converting the array to the required form.
 *
 * @param array $data
 * @return array
 */
function transformButtonsArray(array $data): array
{
    $result = [];
    foreach ($data as $item) {
        $result[$item['name']] = ['Name' => $item['title'], 'URL' => $item['url']];
    }

    return $result;
}


/* @param $var
 * print_r and debug_backtrace to it
 */
function vv($var)
{
    echo '<pre>';
    print_r($var);
    print_r(debug_backtrace());
    echo '</pre>';
    die('end of debug_backtrace');
}

/* @param $var
 * var_dump and die
 */
function vdd($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}

/* @param $var
 * print_r and die
 */
function prd($var)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    die();
}
