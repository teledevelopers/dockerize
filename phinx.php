<?php

use KebaCorp\VaultSecret\VaultSecret;

VaultSecret::load('secrets/secrets.json');

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => VaultSecret::getSecret('MYSQL_HOST'),
            'name' => VaultSecret::getSecret('MYSQL_DATABASE'),
            'user' => VaultSecret::getSecret('MYSQL_USER'),
            'pass' => VaultSecret::getSecret('MYSQL_PASSWORD'),
            'port' => VaultSecret::getSecret('MYSQL_PORT'),
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => 'mysql',
            'host' => VaultSecret::getSecret('MYSQL_HOST'),
            'name' => VaultSecret::getSecret('MYSQL_DATABASE'),
            'user' => VaultSecret::getSecret('MYSQL_USER'),
            'pass' => VaultSecret::getSecret('MYSQL_PASSWORD'),
            'port' => VaultSecret::getSecret('MYSQL_PORT'),
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => 'mysql',
            'host' => VaultSecret::getSecret('MYSQL_HOST'),
            'name' => VaultSecret::getSecret('MYSQL_DATABASE'),
            'user' => VaultSecret::getSecret('MYSQL_USER'),
            'pass' => VaultSecret::getSecret('MYSQL_PASSWORD'),
            'port' => VaultSecret::getSecret('MYSQL_PORT'),
            'charset' => 'utf8',
        ]
    ],
    'version_order' => 'creation'
];
