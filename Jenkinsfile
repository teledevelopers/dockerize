//# telecc/docker-makebuilder-2.2.1

/**
 * This exists primarily because of a bug in Jenkins pipeline that causes
 * any call to the "properties" closure to overwrite all job property settings,
 * not just the ones being set.  Therefore, we set all properties that
 * the generator may have set when it generated this job (or a human).
 *
 * @param settingsOverrides a map, see defaults below.
 * @return
 */
def setJobProperties(Map settingsOverrides = [:]) {
    def settings = [
        discarder_builds_to_keep:'10',
        discarder_days_to_keep: '7',
        cron: null,
        paramsList: [],
        upstreamTrigger: null,
        pollSCMTrigger: null,
        disableConcurrentBuilds: false
    ] + settingsOverrides

    properties([
        buildDiscarder(
            logRotator(
                    artifactDaysToKeepStr: '',
                    artifactNumToKeepStr: '',
                    daysToKeepStr: '7',
                    numToKeepStr: ''
                )
        ),
        disableConcurrentBuilds()
    ])
//    echo "Setting job properties. discarder is '${settings.discarder_builds_to_keep}' and cron is '${settings.cron}' (${settings.cron?.getClass()})"
    def jobProperties = [
        //these have to be strings:
        buildDiscarder(
            logRotator(
                artifactDaysToKeepStr: '',
                artifactNumToKeepStr: '',
                daysToKeepStr: "${settings.discarder_days_to_keep}",
                numToKeepStr: "${settings.discarder_builds_to_keep}"
            )
        )
    ]

    if (settings.cron) {
        jobProperties << upstreamTrigger([cron(settings.cron)])
    }

    if (settings.upstreamTrigger) {
        jobProperties << pipelineTriggers([upstream(settings.upstreamTrigger)])
    }

    if (settings.pollSCMTrigger) {
        jobProperties << pipelineTriggers([pollSCM(settings.pollSCMTrigger)])
    }

    if (settings.disableConcurrentBuilds) {
        jobProperties << disableConcurrentBuilds()
    }

    if (settings.paramsList?.size() > 0) {
        def generatedParams = []
        settings.paramsList.each { //params are specified as name:default:description
            def param_parts = it.split(':', 4).toList() //I need to honor all delimiters but I want a list
            switch(param_parts[0]) {
                case "credentials":
                    generatedParams <<  credentials(
                        name: "${param_parts[1]}",
                        defaultValue: "${param_parts[2] ?: ''}",
                        description: "${param_parts[3]}",
                        credentialType: "Username with password",
                        required: true
                    )
                    break
                case "string":
                    generatedParams << string(
                        name: "${param_parts[1]}",
                        defaultValue: "${param_parts[2] ?: ''}",
                        description: "${param_parts[3] ?: ''}",
                        trim: true
                    )
                    break
                case "boolean":
                    generatedParams << booleanParam(
                        name: "${param_parts[1]}",
                        defaultValue: "${param_parts[2] ?: ''}",
                        description: "${param_parts[3] ?: ''}",
                        trim: true)
                    break
                default:
                    throw new Exception("Unknown type \"${param_parts[0]}\" in paramsList().")
                    break
            }
        }
        jobProperties << parameters(generatedParams)
    }

    // echo "Setting job properties: ${jobProperties}"

    properties(jobProperties)
}

pipeline {
   agent any

    options {
        disableConcurrentBuilds()
    }

    environment {
        DOCKER_REPO_URL             = "https://tc-dockerhub.telecontact.ru/v2/"
        DOCKER_REGISTRY_MIRROR      = 'tc-dockerhub.telecontact.ru/mirror/'
        DOCKER_RELEASE_REPO         = 'tc-dockerhub.telecontact.ru/'
        COMPOSE_FILE                = 'docker-compose.yml:docker-compose.build.yml'
        SONARQUBE_URL               = 'https://tc-msk-sq0.telecontact.ru'
    }

    stages {
        stage('Prepare') {
            steps {
                echo "Prepare"
                setJobProperties(
                    //each of these is optional you may simply need the paramsList and that's it.
                    //discarder_builds_to_keep: "10",
                    //cron: "",
                    paramsList: [
                        // paramsList entry="type:name:defaultValue:description"
                        "credentials:DOCKER_REPO_credentialsId:${params.DOCKER_REPO_credentialsId ?: ''}:",
                        "credentials:SONARQUBE_LOGIN_credentialsId:${params.SONARQUBE_LOGIN_credentialsId ?: ''}:",
                        "boolean:SONARQUBE_SCAN_ENABLE:${params.SONARQUBE_SCAN_ENABLE ?: false}:"
                    ],
                    disableConcurrentBuilds: true,
                    //upstreamTrigger: 'some-job',
                    pollSCMTrigger: '*/5 *  * * *'
                )
                script {
                    if ( params.DOCKER_REPO_credentialsId == '' || params.DOCKER_REPO_credentialsId == null ) {

                        echo "Error, params.DOCKER_REPO_credentialsId empty, configure it immediately!"
                        throw new Exception("Error, params.DOCKER_REPO_credentialsId empty")
                    }
                    if ( params.SONARQUBE_SCAN_ENABLE == true ) {
                        if ( params.SONARQUBE_LOGIN_credentialsId == '' || params.SONARQUBE_LOGIN_credentialsId == null ) {

                            echo "Error, params.SONARQUBE_LOGIN_credentialsId empty, configure it immediately!"
                            throw new Exception("Error, params.SONARQUBE_LOGIN_credentialsId empty")
                        }
                    }
                }
                script {
                    //hostfix for empty BRANCH_NAME in noMultibranch project
                    if ( env.BRANCH_NAME == '' || env.BRANCH_NAME == null ) {
                        env.BRANCH_NAME = env.GIT_BRANCH.replaceAll("origin\\/(.*)","\$1")
                    }
                    withDockerRegistry([
                        credentialsId: "${params.DOCKER_REPO_credentialsId}",
                        url: "${DOCKER_REPO_URL}",
                    ]) {
                        sh 'make lint'
                    }
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    withDockerRegistry([
                        credentialsId: "${params.DOCKER_REPO_credentialsId}",
                        url: "${DOCKER_REPO_URL}",
                    ]) {
                        sh 'touch .env'
                        sh 'make'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                script {
                    withDockerRegistry([
                        credentialsId: "${params.DOCKER_REPO_credentialsId}",
                        url: "$DOCKER_REPO_URL",
                    ]) {
                        sh 'echo "TODO: Warning! Test not found"'
                        if ( params.SONARQUBE_SCAN_ENABLE == '' || params.SONARQUBE_SCAN_ENABLE == null  || params.SONARQUBE_SCAN_ENABLE == false ) {
                            sh 'echo "skip socarqube scan, param SONARQUBE_SCAN_ENABLE is false"'
                        } else {
                            withCredentials([string(credentialsId: "${params.SONARQUBE_LOGIN_credentialsId}", variable: 'SONARQUBE_LOGIN')]) {
                                sh 'echo "run socarqube scan"'
                                sh 'make sonar_scan'
                            }
                        }
                    }
                }
            }
        }
        stage('Release') {
            steps {
                script {
                    withDockerRegistry([
                        credentialsId: "${params.DOCKER_REPO_credentialsId}",
                        url: "$DOCKER_REPO_URL"
                    ]) {
                        sh 'make release'
                    }
                }
            }
        }
        stage('Clenup') {
            steps {
                script {
                    sh 'make clean'
                }
            }
        }
    }
}