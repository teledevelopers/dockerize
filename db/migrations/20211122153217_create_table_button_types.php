<?php

declare(strict_types = 1);

use Phinx\Migration\AbstractMigration;

final class CreateTableButtonTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $rows = [['id'        => 1,
                  'name'      => 'Кнопка',
                  'call_type' => 'outbound'
                 ],
                 ['id'        => 2,
                  'name'      => 'Тема',
                  'call_type' => 'inbound'
                 ]
        ];

        $table = $this->table('button_types', ['id' => true, 'primary_key' => 'id']);

        $table->changeColumn('id', 'integer', ['identity' => true])
            ->addColumn('name', 'string', ['limit' => 50])
            ->addColumn('call_type', 'string', ['limit' => 50])
            ->create();

        $table->insert($rows);
        $table->saveData();
    }
}
