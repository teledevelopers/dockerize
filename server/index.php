<?php
require_once 'functions.php';
require 'DB.class.php';
require __DIR__ . '/../vendor/autoload.php';

use KebaCorp\VaultSecret\VaultSecret;

VaultSecret::load(__DIR__ . '/../secrets/secrets.json');

$action = $_REQUEST['action'] ?? "";
$response = [];

try {
    $response =[
        'type' => 'success',
        'data' => []
    ];
    switch ($action) {
        case 'getAllButtons':
            $response['data'] = DB::Instance()->HardQuery(
                'SELECT o.name,ul.title,ul.url FROM objects o
                                                    LEFT JOIN objects_to_url_list otul ON o.id = otul.obj_id
                                                    LEFT JOIN url_list ul ON ul.id = otul.url_id
                                                    WHERE o.type = 1
                                                    AND o.is_active = 1',
                true
            );
            $response['data'] = transformButtonsArray($response['data']);
            return json_encode($response, JSON_FORCE_OBJECT);
        case 'getUrlList':
            $response['data'] = DB::Instance()->SelectWithKey('url_list');
            break;
//    case 'getAllObjects':
//        $data = DB::Instance()->SelectWithKey('objects');
//        break;
        case 'getButtonsList':
            $response['data'] = DB::Instance()->getObjectsListByType(1);
            break;
        case 'getThemesList':
            $response['data'] = DB::Instance()->getObjectsListByType(2);
            break;
        case 'updateUrl':
            $query = "Update url_list ";
            $response['data'] = '';
            break;
        case 'updateObject':
            $query = "Update object ";
            $response['data'] = '';
            break;
        case 'updateRelation':
            $response['data'] = DB::Instance()->Update('objects_to_url_list', ['url_id'=>$_REQUEST['urlId']], 'id', $_REQUEST['relationId']);
            break;
        case 'deleteRelation':
            $response['data'] = DB::Instance()->Delete('objects_to_url_list', 'id', $_REQUEST['relationId']);
            break;
        default:
            $response['data'] =[];
    }

} catch (Exception $e){
    $response = [
        'type' => 'error',
        'message' => $e->getMessage()
    ];
}

echo json_encode($response);
