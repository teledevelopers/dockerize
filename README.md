# Route Management Service

Для запуска проекта введите команды...

```
make build
make run
```

Для запуска проекта в браузере перейдите по адресу http://localhost:{DEV_PORT_HOST}

#### Управление миграциями
```
php vendor/robmorgan/phinx/bin/phinx create CreateTableMessages
php vendor/robmorgan/phinx/bin/phinx migrate
php vendor/robmorgan/phinx/bin/phinx rollback
```

#### Документация по миграциям
https://tretyakov.net/post/phinx-migracii-bazy-dannyh/
https://book.cakephp.org/phinx/0/en/migrations.html


