<?php

require __DIR__ . '/../vendor/autoload.php';

use KebaCorp\VaultSecret\VaultSecret;

VaultSecret::load(__DIR__ . '/../secrets/secrets.json');

/**
 * Class DB
 */
class DB
{
    private static $instance;

    private PDO    $db;

    public static function Instance(): DB
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        setlocale(LC_ALL, 'ru_RU.UTF8');
        $this->db = new PDO('mysql' . ':host=' . VaultSecret::getSecret('MYSQL_HOST') . ';dbname='
            . VaultSecret::getSecret('MYSQL_DATABASE'), VaultSecret::getSecret('MYSQL_USER'),
            VaultSecret::getSecret('MYSQL_PASSWORD'));
        $this->db->exec('SET NAMES UTF8');
        $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    /**
     * Обрабатывает любой Select запрос, но он должен быть полностью написан вручную.
     *
     * @param string $query
     * @param bool   $getAll fetchAll()/fetch()
     *
     * @return array|mixed
     */
    public function HardQuery(string $query, bool $getAll = false)
    {
        $q = $this->db->prepare($query);
        $q->execute();

        if ($q->errorCode() != PDO::ERR_NONE) {
            $info = $q->errorInfo();
            throw new \PDOException($info[2]);
        }
        if ($getAll) {
            return $q->fetchAll();
        }

        return $q->fetch();
    }

    /**
     * @param string $query
     *
     * @return int
     */
    public function rawQuery(string $query): int
    {
        $q = $this->db->prepare($query);

        return $q->execute();
    }

    /**
     * @param string $table
     * @param string $where_key
     * @param string $where_value
     * @param bool   $getAll fetchAll()/fetch()
     *
     * @return array|mixed
     * @example Select('table', 'id', 1)  // Select('table')
     */
    public function SelectWithKey(string $table, string $where_key = '', string $where_value = '', bool $getAll = false)
    {
        if ($where_key and $where_value) {
            $query = "SELECT * FROM {$table} WHERE {$where_key} =  {$where_value}";
        } else {
            $query = "SELECT * FROM {$table}";
        }

        $q = $this->db->prepare($query);
        $q->execute();

        if ($q->errorCode() != \PDO::ERR_NONE) {
            $info = $q->errorInfo();
            throw new \PDOException($info[2]);
        }

        if ($getAll) {
            return $q->fetchAll();
        } else {
            if ($where_key and $where_value) {
                return $q->fetch();
            } else {
                return $q->fetchAll();
            }
        }
    }

    /**
     * @param string $table
     * @param array  $array
     *
     * @return string
     * @example  Insert("goods",['title'=>'Товар 1','price'=>100])
     */
    public function Insert(string $table, array $array): string
    {
        $columns = [];

        foreach ($array as $key => $value) {
            $columns[] = $key;
            $masks[] = ":$key";

            if ($value === null) {
                $array[$key] = 'NULL';
            }
        }

        $columns_s = implode(',', $columns);
        $masks_s = implode(',', $masks);

        $query = "INSERT INTO {$table} ({$columns_s}) VALUES ({$masks_s})";

        $q = $this->db->prepare($query);
        $q->execute($array);

        if ($q->errorCode() != \PDO::ERR_NONE) {
            $info = $q->errorInfo();
            throw new \PDOException($info[2]);
        }

        return $this->db->lastInsertId();
    }

    /**
     * Update('table', ['count' => 10,'price'=>1000], 'id', '2')
     *
     * @param string $table
     * @param array  $array
     * @param string $where_key
     * @param string $where_value
     *
     * @return int|array
     * @example Update('table', ['count' => 10,'price'=>1000], 'id', '2')
     */
    public function Update(string $table, array $array, string $where_key = '', string $where_value = ''): int
    {
        $sets = [];
        foreach ($array as $key => $value) {
            $sets[] = "$key=:$key";

            if ($value === null) {
                $array[$key] = 'NULL';
            }
        }

        $sets_s = implode(',', $sets);

        $query = "UPDATE {$table} SET {$sets_s} WHERE {$where_key} = {$where_value}";

        $q = $this->db->prepare($query);
        $q->execute($array);
        if ($q->errorCode() != \PDO::ERR_NONE) {
            throw new \PDOException($q->errorInfo()[2]);
        }

        return $q->rowCount();
    }

    /**
     * @param string $table
     * @param string $where_key
     * @param string $where_value
     *
     * @return int
     * @example Delete('table', 'id', '2')
     */
    public function Delete(string $table, string $where_key = '', string $where_value = ''): int
    {
        $query = "DELETE FROM {$table} WHERE {$where_key} = {$where_value}";
        $q = $this->db->prepare($query);
        $q->execute();

        if ($q->errorCode() != \PDO::ERR_NONE) {
            $info = $q->errorInfo();
            throw new \PDOException($info[2]);
        }

        return $q->rowCount();
    }

    /**
     * @param int $type 1-buttons 2-themes
     *
     * @return array
     */
    public function getObjectsListByType(int $type): array
    {
        if (!in_array($type, [1, 2])) {
            throw new \PDOException('Incorrect type of object!');
        }
        $query = "
                SELECT o.name,
                       o.type,
                       otul.id     AS relation_id,
                       otul.updated_at as updated_at,
                       ul.id    AS url_id,
                       ul.title AS url_title,
                       ul.url   AS url_link
                FROM objects_to_url_list otul
                         LEFT JOIN objects o ON o.id = otul.obj_id
                         LEFT JOIN url_list ul ON ul.id = otul.url_id
                WHERE type = {$type}
                AND o.is_active = 1
        ";
        $q = $this->db->prepare($query);
        $q->execute();

        if ($q->errorCode() != \PDO::ERR_NONE) {
            $info = $q->errorInfo();
            throw new \PDOException($info[2]);
        }

        return $q->fetchAll();
    }

    private function __sleep()
    {
    }

    private function __wakeup()
    {
    }

    private function __clone()
    {
    }
}
