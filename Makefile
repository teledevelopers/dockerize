# telecc/docker-makebuilder-2.2.1
.EXPORT_ALL_VARIABLES:

CC := docker-compose
DOCKER_BIN := docker
MAKEBUILDER_HELPER_IMAGE=tc-dockerhub.telecontact.ru/telecc-docker-makebuilder/helper
MAKEBUILDER_HELPER_VERSION := 0.1
VV := docker run --tty --rm ${MAKEBUILDER_HELPER_IMAGE}:${MAKEBUILDER_HELPER_VERSION} /opt/makebuilder-helper/version.sh

ifneq (,$(wildcard .env))
	include .env
	export $(shell sed 's/=.*//' .env)
endif

ifeq ($(META_FILE), )
	ifneq ("$(wildcard package.json)","")
		META_FILE = package.json
	endif
endif

ifeq ($(META_FILE), )
	ifneq ("$(wildcard composer.json)","")
		META_FILE = composer.json
	endif
endif

PRODUCT_NAME           := $(if $(PRODUCT_NAME),$(PRODUCT_NAME),$(shell cat ${META_FILE} | jq --raw-output '.name' | sed 's/\//-/g' ))
DOCKER_REGISTRY_MIRROR := $(if $(DOCKER_REGISTRY_MIRROR),$(DOCKER_REGISTRY_MIRROR),tc-dockerhub.telecontact.ru/mirror/)
DOCKER_RELEASE_REPO    := $(if $(DOCKER_RELEASE_REPO),$(DOCKER_RELEASE_REPO),tc-dockerhub.telecontact.ru/)

DOCKER_REPO     := $(if $(DOCKER_REPO),$(DOCKER_REPO),tc-dockerhub.telecontact.ru/)
DOCKER_REPO_URL := "https://${DOCKER_REPO}/v2/"
COMPOSE_FILE    := $(if $(COMPOSE_FILE),$(COMPOSE_FILE),docker-compose.yml:docker-compose.build.yml)

BRANCH_NAME := $(if $(BRANCH_NAME),$(shell echo $(BRANCH_NAME) | sed 's/\//./g' | sed 's/_/-/g' ),$(shell git rev-parse --abbrev-ref HEAD | sed 's/\//./g' | sed 's/_/-/g' 2>/dev/null))
BUILD_ID    := $(if $(BUILD_ID),$(BUILD_ID),local)

PRODUCT_VERSION  := $(shell cat ${META_FILE} | jq --raw-output '.version')
PRODUCT_REVISION := $(shell cat ${META_FILE} | jq --raw-output '.revision//""')

ifeq ($(PRODUCT_REVISION),)
	DESCRIBE_LONG := $(PRODUCT_VERSION)
else
	DESCRIBE_LONG := $(PRODUCT_VERSION)-$(PRODUCT_REVISION)
endif

TAG_DISCOVERY_MOD := unknown
BRANCH_NAME_CLASS := $(shell echo $(BRANCH_NAME) | sed 's/release\..*/release/g' | sed 's/^feature\..*/feature/g')

ifeq ($(VERSION), )

	META_SHA := $(shell git log --pretty=format:'%h' -n 1 2>/dev/null)

	ifeq ($(BRANCH_NAME_CLASS), $(filter $(BRANCH_NAME_CLASS), master HEAD release))

		ifeq ($(BRANCH_NAME_CLASS), $(filter $(BRANCH_NAME_CLASS), master HEAD))

			TAG_DISCOVERY_MOD := MASTER
			VERSION_SHORT     := $(shell $(VV) get ci_short ${DESCRIBE_LONG})
			VERSION_LONG      := ${VERSION_SHORT}+${BRANCH_NAME_CLASS}.sha.${META_SHA}.build.$(BUILD_ID)
			VERSION_BRIEF     := $(shell $(VV) get ci_brief ${DESCRIBE_LONG})

		else ifeq ($(BRANCH_NAME_CLASS), $(filter $(BRANCH_NAME_CLASS), release))

			TAG_DISCOVERY_MOD := RELEASE
			VERSION_SHORT     := $(shell $(VV) get ci_short ${DESCRIBE_LONG})+${BRANCH_NAME}.sha.${META_SHA}
			VERSION_LONG      := ${VERSION_SHORT}.build.$(BUILD_ID)

		endif

		BUILD_INFO          := ${BRANCH_NAME}.sha.${META_SHA}.build.$(BUILD_ID)

	else

		TAG_DISCOVERY_MOD   := AUTO
		ifeq ($(META_SHA), )
			BUILD_INFO    := ${BRANCH_NAME}.build.$(BUILD_ID)
			VERSION_SHORT := $(shell $(VV) get ci_long ${DESCRIBE_LONG})+${BRANCH_NAME}
		else
			BUILD_INFO    := ${BRANCH_NAME}.sha.${META_SHA}.build.$(BUILD_ID)
			VERSION_SHORT := $(shell $(VV) get ci_long ${DESCRIBE_LONG})+${BRANCH_NAME}.sha.${META_SHA}
		endif
		VERSION_LONG  := ${VERSION_SHORT}.build.$(BUILD_ID)

	endif
else
	TAG_DISCOVERY_MOD   := DISABLED
	BUILD_INFO          := build.$(BUILD_ID)
	VERSION_SHORT       := $(VERSION)
	VERSION_LONG        := $(VERSION).build.$(BUILD_ID)
endif

VERSION_DOCKER_SHORT := $(shell echo $(VERSION_SHORT) | sed 's/\+/-/g')
VERSION_DOCKER_BRIEF := $(shell echo $(VERSION_BRIEF) | sed 's/\+/-/g')
VERSION_DOCKER_LONG  := $(shell echo $(VERSION_LONG)  | sed 's/\+/-/g')

DOCKER_BUILD_TAGS =
DOCKER_BUILD_TAGS += ${VERSION_DOCKER_LONG}

SONAR_PROJECT_KEY = $(PRODUCT_NAME):$(BRANCH_NAME)
SONAR_SCANNER_IMAGE := $(if $(SONAR_SCANNER_IMAGE),$(SONAR_SCANNER_IMAGE),sonarsource/sonar-scanner-cli:4.6)

ifneq ($(BUILD_ID),local)
	DOCKER_BUILD_TAGS += ${VERSION_DOCKER_SHORT}
	ifeq ($(BRANCH_NAME), $(filter $(BRANCH_NAME), master))
		DOCKER_BUILD_TAGS += latest
		DOCKER_BUILD_TAGS += ${VERSION_DOCKER_BRIEF}
	endif
	ifeq ($(BRANCH_NAME_CLASS), $(filter $(BRANCH_NAME_CLASS), release))
		DOCKER_BUILD_TAGS += latest_release
	endif
	ifeq ($(BRANCH_NAME), $(filter $(BRANCH_NAME), develop))
		DOCKER_BUILD_TAGS += develop
	endif
	ifeq ($(BRANCH_NAME_CLASS), $(filter $(BRANCH_NAME_CLASS), feature))
		DOCKER_BUILD_TAGS +=  $(BRANCH_NAME)
	endif
endif

all: lint build

build: prebuild build_docker_image

build_lint: print_docker_compose_config_build  lint_print_vars lint_version lint_deprecations

build_docker_image:
	env \
		VERSION=$(VERSION_DOCKER_LONG) \
		DOCKER_RELEASE_REPO=localhost/ \
		DOCKER_REGISTRY_MIRROR=$(DOCKER_REGISTRY_MIRROR) \
		$(CC) build --pull --no-cache

	for DOCKER_BUILD_TAG in $(DOCKER_BUILD_TAGS) ; \
	do \
		env \
		VERSION=$$DOCKER_BUILD_TAG \
		$(CC) build; \
	done

prebuild:
# 	start prebuild >>
	@prebuild_services_list=`VERSION=$(VERSION_DOCKER_LONG) \
		DOCKER_RELEASE_REPO=localhost/ \
		DOCKER_REGISTRY_MIRROR=$(DOCKER_REGISTRY_MIRROR) \
		$(CC) config --services | grep -E '^prebuild_' | tr "\n" " "`; \
	if [ ! -z "$$prebuild_services_list" ]; then \
		echo $$prebuild_services_list; \
		for prebuild_service in $$prebuild_services_list ; \
		do \
			echo ${prebuild_service}; \
			env \
			VERSION=$(VERSION_DOCKER_LONG) \
			DOCKER_RELEASE_REPO=localhost/ \
			DOCKER_REGISTRY_MIRROR=$(DOCKER_REGISTRY_MIRROR) \
			${CC} run --rm $$prebuild_service; \
		done \
	else \
		echo "prebuild_services_list empty" ; \
	fi
# 	<< end prebuild

lint: build_lint

lint_print_vars:
	# PRODUCT_NAME = ${PRODUCT_NAME}
	# BRANCH_NAME  = ${BRANCH_NAME}
	#
	# BRANCH_NAME_CLASS   = ${BRANCH_NAME_CLASS}
	# DESCRIBE_LONG       = ${DESCRIBE_LONG}
	# DOCKER_RELEASE_REPO = ${DOCKER_RELEASE_REPO}
	# DOCKER_REGISTRY_MIRROR = ${DOCKER_REGISTRY_MIRROR}
	# TAG_DISCOVERY_MOD   = ${TAG_DISCOVERY_MOD}
	#
	# BUILD_INFO = ${BUILD_INFO}
	# META_SHA   = ${META_SHA}
	#
	# VERSION_SHORT = ${VERSION_SHORT}
	# VERSION_LONG  = ${VERSION_LONG}
	#
	# VERSION_DOCKER_SHORT = ${VERSION_DOCKER_SHORT}
	# VERSION_DOCKER_LONG  = ${VERSION_DOCKER_LONG}
	# VERSION_DOCKER_BRIEF = ${VERSION_DOCKER_BRIEF}
	#
	# DOCKER_BUILD_TAGS = ${DOCKER_BUILD_TAGS}
	# DOCKER_BUILD_TAG:
		@for DOCKER_BUILD_TAG in $(DOCKER_BUILD_TAGS) ; do echo "#  - $$DOCKER_BUILD_TAG"; done
	#
	# SONAR_PROJECT_KEY = $(SONAR_PROJECT_KEY)
	# ======================================

lint_version:
	@$(VV) validate $(VERSION_SHORT)
	@$(VV) validate $(VERSION_LONG)
	# ======================================
	@jq --version
	# ======================================
	${DOCKER_BIN} version
	# ======================================

lint_deprecations:
	# Deprecations:
	@[ -f "version.sh" ] && echo "[DeprecationWarning]: delete 'version.sh' from project" || true
	# ======================================

print_docker_compose_config_build:
	env \
		VERSION=$(VERSION_DOCKER_LONG) \
		DOCKER_RELEASE_REPO=localhost/ \
		DOCKER_REGISTRY_MIRROR=$(DOCKER_REGISTRY_MIRROR) \
		$(CC) config

run:
	env \
		VERSION=$(VERSION_DOCKER_LONG) \
		$(CC) up \
			--always-recreate-deps \
			--detach \
			--remove-orphans

stop:
	env \
		VERSION=$(VERSION_DOCKER_LONG) \
		$(CC) stop

clean:
	env \
		VERSION=$(VERSION_DOCKER_LONG) \
		DOCKER_RELEASE_REPO=localhost/ \
		DOCKER_REGISTRY_MIRROR=$(DOCKER_REGISTRY_MIRROR) \
		$(CC) down --rmi all

	for DOCKER_BUILD_TAG in $(DOCKER_BUILD_TAGS) ; \
	do \
		env \
			VERSION=$$DOCKER_BUILD_TAG \
			$(CC) down --rmi all ;\
	done

release: release_docker_image

release_docker_image:
	for DOCKER_BUILD_TAG in $(DOCKER_BUILD_TAGS) ; \
	do \
		env \
		VERSION=$$DOCKER_BUILD_TAG \
		$(CC) push; \
	done

sonar_scan:
	@docker run \
		--rm \
		-e SONAR_HOST_URL="$(SONARQUBE_URL)" \
		-e SONAR_LOGIN="$(SONARQUBE_LOGIN)" \
		-v "$(shell pwd):/usr/src" \
		$(DOCKER_REGISTRY_MIRROR)$(SONAR_SCANNER_IMAGE) \
			-Dsonar.projectKey=$(SONAR_PROJECT_KEY)
