<?php

use yii\db\Migration;

/**
 * Class m210513_125144_crontab
 */
class m000000_001620_crontab extends Migration
{
    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';

        $this->createTable(
            'crontab',
            [
                'id'      => $this->primaryKey(11),
                'time'    => $this->string(128)->notNull(),
                'task'    => $this->string(2048)->notNull(),
                'comment' => $this->string(2048)->notNull(),
                'active'  => $this->tinyInteger(1)->notNull()->defaultValue(1),
            ], $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('crontab');
    }
}
