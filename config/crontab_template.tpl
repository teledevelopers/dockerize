# crontab for docker app

SHELL=/bin/sh
PATH=/var/www/html:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
LYNX=/usr/bin/lynx -source

#Project mailgroup or
MAILFROM=helloworld@telecontact.ru

#Project mailgroup or
MAILTO=helloworld@telecontact.ru

CONTENT_TYPE="text/plain; charset=UTF-8"
CONTENT_TRANSFER_ENCODING=8bit
PYTHONIOENCODING=utf-8
LANG=ru_RU.UTF-8

{{ env "APP_CRON_TASKS" | base64Decode }}

#end
