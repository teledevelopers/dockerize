<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTableObjectsToUrlList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('objects_to_url_list', ['id' => true, 'primary_key' => 'id']);

        $table->changeColumn('id', 'integer', ['identity' => true])
            ->addColumn('obj_id', 'integer')
            ->addColumn('url_id', 'integer')
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['obj_id'], ['unique' => true, 'name' => 'idx_obj_id'])
            ->create();
    }
}
